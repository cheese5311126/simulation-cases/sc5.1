# Towards the European tephra hazard model

**SC leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/INGV.png?inline=false" width="50">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/csic.png?inline=false" width="80">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/IMO.png?inline=false" width="80">

**Codes:** [FALL3D](https://gitlab.com/cheese5311126/codes/fall3d)

**Target EuroHPC Architectures:** MN5, Leonardo

**Type:** Hazard assessment

## Description

This capacity SC aims at deriving the first homogeneous multi-volcano
probabilistic hazard model for volcanic tephra fallout in Europe. Due to data and
knowledge heterogeneities, the main challenge relates to including multiple
sources (i.e. volcanoes or volcanic districts) and, for each of them, capturing all
possible eruptive scenarios relevant for tephra dispersal and fallout. Where the
knowledge of volcanic sources is at sufficient detail (e.g. eruptive record and
possible scenarios), this SC will produce a regional tephra hazard model at high
resolution, considering the main tephra-producing volcanoes in the territory
(e.g., for Italy, Vesuvius, Campi Flegrei, Ischia and Etna). Such a model will
identify, in probabilistic terms, those areas in the region more likely to receive
tephra fallout. On the other hand, at European scale, a similar approach will be
used to provide a European hazard model for airspace areas prone to be
contaminated by hazardous concentrations at cruise Flight Level. Tephra-dispersion model results for eruptions in several volcanic districts in Europe (Iceland; Italy; Canaries, Spain; Jan Mayen, Norway; Greece; and Azores, Portugal) will be integrated into a single homogeneous probabilistic model, with an incremental methodology that allow adding further volcanoes in
the future. Eruptive scenarios for these volcanic districts will be supported by the
European Catalog of Volcanoes (a database produced during the EUROVOLC
project).

<img src="SC5.1.png" width="800">

**Figure 3.7.1.** Graphical abstract of SC5.1 highlighting: (i) the domain for the
European-scale application (at 10 km resolution) in red solid lines, enclosed by the 25N
and 75N parallels and 32W and 38E meridians; (ii) the high-priority volcanoes that will
be considered in this SC, in red triangles; (iii) the low-priority volcanoes in yellow
triangles (that will be considered depending on the resources available); (iv) the
domains of the regional-scale applications in small red and yellow rectangles show (high-
and low-priority respectively).

## Expected results

First step towards a European volcanic ash hazard assessment in line with the United Nations DDR policy.
The analysis will be performed over a domain including the densest air traffic regions in Europe, which spans over a 2000 x 5000 km area at a resolution of less than 10 km for European scale and 2 km at regional scale. The final datasets will be accessible through the TCS Volcano Observations service in EPOS for their usage and distribution.